#!/usr/bin/perl
#parsing interface and ip address from 'ip a' command
$var = `ip a`;
@interface = ($var =~ m/\d:\s(\S+?):/g);
@address = ($var =~ m/\sinet\s(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})/g);
%hash;
for(my $i=0; $i<scalar @interface;$i++){
$hash{$interface[$i]} = $address[$i];
}

print "\ninterface\t\taddress\n";

foreach my $key ( keys %hash )
{
  print $key."\t\t\t".$hash{$key}."\n";
}
