#!/usr/bin/python
#signed and unsigned domains listing
import re
import subprocess
import MySQLdb
from termcolor import colored



si = []
total = []
unsigned = []
#folder = raw_input('enter folder name:')
folder = '/home/rnd/Desktop/zone'
string = subprocess.Popen("ls"+" "+folder, shell=True, stdout=subprocess.PIPE).stdout.read()
signed = re.findall(r'(\S*.db.signed)', string)
files = re.findall(r'(\S*.db)', string)
for i in signed:
	sign = i.split(".signed")
	si.append(sign[0])
 
print colored("\n\t::SIGNED::\n", 'red')

for i in si:
	print "\t"+colored(i,'green')+colored("::SIGNED", 'red')

for i in files:
	if i not in si:
		unsigned.append(i)

print colored("\n\t::UNSIGNED::\n", 'red')

for i in unsigned:
	print "\t"+colored(i,'green')+colored("::UNSIGNED", 'red')

print "\n"


db = MySQLdb.connect("localhost","testuser","test623","zone" )
cursor = db.cursor()
for i in si:
	sql = "INSERT INTO zone(domain,status)VALUES ('"+i+"', 'signed')"
	try:
   		cursor.execute(sql)
   		db.commit()
	except:
   		db.rollback()


for i in unsigned:
   	sql = "INSERT INTO zone(domain,status)VALUES ('"+i+"', 'unsigned')"
   	try:
   		cursor.execute(sql)
   		db.commit()
	except:
   		db.rollback()
db.close()

