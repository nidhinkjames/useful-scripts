#python script for finding ip address and interfaces
import subprocess
import re
list = []
string = subprocess.Popen("ip a", shell=True, stdout=subprocess.PIPE).stdout.read()
#print string
interfaces = re.findall(r'\d:\s(\S*):', string)
address = re.findall(r'inet\s(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})', string)
# If-statement after search() tests if it succeeded
for i in range(len(interfaces)):
	list.append({interfaces[i]:address[i]})

print "\n\nInterfaces	IP\n"
for data in list:
	for key,value in data.items():
		print key+"		"+value


print"\n"
